﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class Admanager : MonoBehaviour
{
    public string InterstitialAdID;

    InterstitialAd MyinterstitialAd;

    private void Start()
    {
        MyinterstitialAd = new InterstitialAd(InterstitialAdID);
    }

    public void LoadInsterstitialAd()
    {
        AdRequest requeset = new AdRequest.Builder().Build();
        MyinterstitialAd.LoadAd(requeset);
    }

    public void showInterstitialAd()
    {
        if (MyinterstitialAd.IsLoaded())
        {
            MyinterstitialAd.Show();
        }
        else
        {
            LoadInsterstitialAd();
        }
    }

}