﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    private Rigidbody rb;
    private Animator anim;
    private GameObject Player;
    private GameObject Flamme;
    private AudioSource SourceAudio;
    private int PTDV;
    public AudioClip FusilFire;
    private GameObject YouReDead;
    private GameObject DontKillHumans;
    public AudioClip DeadSound;
    public AudioClip DontKillHumain;
    private bool killed = false;
    private GameObject cam;
    public GameObject soldierHand;
    
    public bool FireEnCours = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Player = GameObject.Find("Soldier");
        anim = Player.GetComponent<Animator>();
        Flamme = GameObject.Find("Flamme canon");
        Flamme.SetActive(false);
        SourceAudio = GetComponent<AudioSource>();
        YouReDead = GameObject.Find("YouReDead");
        DontKillHumans = GameObject.Find("DontKillHumans");
        cam = GameObject.Find("First Person Camera");
    }

    void FixedUpdate()
    {
        float y = CrossPlatformInputManager.GetAxis("Vertical");
        float x = CrossPlatformInputManager.GetAxis("Horizontal");
        float y2 = CrossPlatformInputManager.GetAxis("VerticalRot");
        float x2 = CrossPlatformInputManager.GetAxis("HorizontalRot");

        Vector3 movement = new Vector3(x, 0.0f, y);
        Vector3 rotation = new Vector3(0, Mathf.Atan2(x2, y2) * Mathf.Rad2Deg, 0);

        //rb.velocity = Vector3.zero;
        //rb.angularVelocity = Vector3.zero;

        bool fire = CrossPlatformInputManager.GetButton("Fire");

        float magni = movement.magnitude;

        //movement = cam.transform.TransformDirection(movement);
        //movement.y = 0.0f;
        //movement.Normalize();
        //movement *= magni;
        //x = movement.x;
        //y = movement.z;

        if (!(x2 == 0 || y2 == 0))
        {
            transform.eulerAngles = rotation;
        }
        else
        {
            rb.angularVelocity = Vector3.zero;
        }

        if (!(x == 0 || y == 0))// && magni >= 0.35f)
        {
            transform.Translate(movement*.01f, Space.World);
            if (magni <= 0.5f)
            {
            //anim.SetTrigger("Walk");
                if (fire)
                {
                    if (!FireEnCours)
                    {
                        anim.SetBool("Idle 0", false);
                        anim.SetBool("Walk 0", false);
                        anim.SetBool("Run 0", false);
                        anim.SetBool("Fire 0", true);
                        StartCoroutine("Fire");
                    }
                }
                else
                {
                    anim.SetBool("Idle 0", false);
                    anim.SetBool("Walk 0", true);
                    anim.SetBool("Run 0", false);
                    anim.SetBool("Fire 0", false);
                    //anim.SetTrigger("Walk");
                    //anim.SetTrigger("StopFire2Walk");
                    Flamme.SetActive(false);
                }
            }
            else
            {
                if (magni > 0.5f)
                {
                    //anim.SetTrigger("Run");
                    if (fire)
                    {
                        if (!FireEnCours)
                        {
                            anim.SetBool("Idle 0", false);
                            anim.SetBool("Walk 0", false);
                            anim.SetBool("Run 0", false);
                            anim.SetBool("Fire 0", true);
                            //anim.SetTrigger("Fire");
                            StartCoroutine("Fire");
                        }
                    }
                    else
                    {
                        anim.SetBool("Idle 0", false);
                        anim.SetBool("Walk 0", false);
                        anim.SetBool("Run 0", true);
                        anim.SetBool("Fire 0", false);
                        //anim.SetTrigger("Run");
                        //anim.SetTrigger("StopFire2Run");
                        Flamme.SetActive(false);
                    }
                }
            }


            //if (magni >= 0.35f) rb.velocity = movement * 0.5f;

            //old version
            //transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(x, y) * Mathf.Rad2Deg, transform.eulerAngles.z);
            //transform.rotation = Quaternion.Euler(0, Mathf.Atan2(x, y) * Mathf.Rad2Deg, 0);

            //x = movement.x;
            //y = movement.z;

            //transform.rotation = Quaternion.LookRotation(new Vector3 (movement.x + x, 0, movement.y + y), Vector3.up);
            //transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(x, y) * Mathf.Rad2Deg, Vector3.up);

            //transform.eulerAngles = new Vector3(0, Mathf.Atan2(x, y) * Mathf.Rad2Deg, 0);
        }
        else
        {
            //anim.SetTrigger("Idle");
            //rb.angularVelocity = Vector3.zero;
            //rb.velocity = Vector3.zero;
            if (fire)
            {
                if (!FireEnCours)
                {
                    anim.SetBool("Idle 0", false);
                    anim.SetBool("Walk 0", false);
                    anim.SetBool("Run 0", false);
                    anim.SetBool("Fire 0", true);
                    //anim.SetTrigger("Fire");
                    StartCoroutine("Fire");
                }
            }
            else
            {
                anim.SetBool("Idle 0", true);
                anim.SetBool("Walk 0", false);
                anim.SetBool("Run 0", false);
                anim.SetBool("Fire 0", false);
                //anim.SetTrigger("Idle");
                //anim.SetTrigger("StopFire2Idle");
                Flamme.SetActive(false);
            }
        }

        PTDV = GetComponent<PointDeVie>().getHP();
        if (PTDV <= 0 && !killed)
        {
            killed = true;
            anim.SetTrigger("Dead");
            YouReDead.GetComponent<Image>().enabled = true;
            SourceAudio.PlayOneShot(DeadSound, 1f);
            StartCoroutine(RetourMenu());
        }
    }
    IEnumerator Fire()
    {
        FireEnCours = true;

        //yield return new WaitForSeconds(1f);

        Flamme.SetActive(true);
        SourceAudio.PlayOneShot(FusilFire, 1f);

        float rayRange = 500.0f;
        Vector3 rayDirection = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;

        if (Physics.Raycast(Flamme.transform.position, rayDirection, out hit, rayRange))
        {
            if (hit.collider.tag == "ZombieOrHumain" && hit.collider.transform.GetComponent<ZombiesOrHumain>().humain)
            {
                hit.collider.gameObject.GetComponent<PointDeVie>().setHP(250);
                StartCoroutine(DoNotKillHumans());
                yield return new WaitForSeconds(1f);
            }
            if (hit.collider.tag == "ZombieOrHumain" && !hit.collider.transform.GetComponent<ZombiesOrHumain>().humain)
            {
                hit.collider.gameObject.GetComponent<PointDeVie>().setHP(hit.collider.gameObject.GetComponent<PointDeVie>().getHP() - 20);
            }
        }

        yield return new WaitForSeconds(0.15f);
        Flamme.SetActive(false);
        yield return new WaitForSeconds(0.15f);

        FireEnCours = false;
    }
    IEnumerator DoNotKillHumans()
    {
        DontKillHumans.GetComponent<Image>().enabled = true;
        SourceAudio.PlayOneShot(DontKillHumain, 1.2f);
        yield return new WaitForSeconds(1f);
        DontKillHumans.GetComponent<Image>().enabled = false;
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Vide") GetComponent<PointDeVie>().setHP(0);
    }
    IEnumerator RetourMenu()
    {
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(0);
    }
}