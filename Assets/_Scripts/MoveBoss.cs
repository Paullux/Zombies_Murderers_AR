﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveBoss : MonoBehaviour {

    private GameObject Player;
    private GameObject Canvas;
    private Vector3 DirectionAPrendre;
    private Vector3 direction;
    private Rigidbody rb;
    public GameObject DeamonEnCours;
    private bool DeamonEaten = false;
    private bool DeamonsAttack = false;
    private bool DeamonTouch = false;
    private GameObject Film;
    private bool FirstMarche = true;
    private bool FirstAttack = true;
    private bool FirstSound = true;

    private GameObject YouWin;

    private AudioSource Source;
    public AudioClip Eaten;
    public AudioClip YouWinSound;
    public GameObject Sortie;

    private float VelociteduDeamon;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        Source = GetComponent<AudioSource>();
        Canvas = GameObject.Find("InterfaceUtilisateur");
        YouWin = GameObject.Find("YouWin");
        Film = GameObject.Find("RawImage");
    }
	
	// Update is called once per frame
	void Update () {
        if (Film.GetComponent<VideoOver>().videoOver)
        {
            Player = GameObject.Find("First Person Camera");
            VelociteduDeamon = GetComponent<PointDeVie>().getHP() / 1000f;
            if (GetComponent<PointDeVie>().getHP() <= 0)
            {
                DeamonEnCours.GetComponent<Animator>().SetBool("Dead", true);
                rb.angularVelocity = Vector3.zero;
                rb.velocity = Vector3.zero;
                if (FirstSound)
                {
                    YouWin.GetComponent<Image>().enabled = true;
                    Source.PlayOneShot(YouWinSound, 1f);
                    FirstSound = false;
                }
                StartCoroutine("RetourMenu");
            }
            else
            {
                DirectionAPrendre = Player.transform.position - transform.position;
                direction = (new Vector3(DirectionAPrendre.x, 0, DirectionAPrendre.z)).normalized;
                rb.velocity = direction * VelociteduDeamon;
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg, transform.eulerAngles.z);
                StartCoroutine("DeamonWalk");
            }
        }
        if (GetComponent<PointDeVie>().getHP() <= 0) DeamonEnCours.GetComponent<Animator>().SetBool("Dead", true);
    }
    IEnumerator SoundBlessure()
    {
        DeamonEaten = true;
        while (DeamonTouch)
        {
            yield return new WaitForSeconds(1.025f);
            Source.PlayOneShot(Eaten, 1.1f);
        }
        DeamonEaten = false;
    }
    IEnumerator DeamonWalk()
    {
        //DeamonEnCours.GetComponent<Animator>().ResetTrigger("Walk");
        if (FirstMarche)
        {
            DeamonEnCours.GetComponent<Animator>().SetTrigger("Walk");
            FirstMarche = false;
        }
        yield return null;
    }
    IEnumerator DeamonAttack(Collision other)
    {
        DeamonsAttack = true;
        rb.angularVelocity = Vector3.zero;

        DeamonEnCours.GetComponent<Animator>().SetTrigger("attackOff");
        DeamonEnCours.GetComponent<Animator>().SetTrigger("attackOn");
        if (!DeamonEaten) StartCoroutine("SoundBlessure");
        if (other.gameObject == Player && FirstAttack && GetComponent<PointDeVie>().getHP() > 0)
        {
            Canvas.GetComponent<PointDeVie>().setHP(Canvas.GetComponent<PointDeVie>().getHP() - 1);
            FirstAttack = false;
        }
        yield return new WaitForSeconds(1);
        DeamonsAttack = false;
        FirstAttack = true;
    }
    void OnCollisionStay(Collision other)
    {
        if (other.gameObject == Player) StartCoroutine("DeamonAttack", other);

        rb.angularVelocity = Vector3.zero;
        rb.velocity = Vector3.zero;
    }
    void OnCollisionExit(Collision other)
    {
        if (other.gameObject == Player)
        {
            StopCoroutine("DeamonAttack");
            DeamonEnCours.GetComponent<Animator>().SetTrigger("attackOff");
            DeamonsAttack = false;

        }
    }
    IEnumerator RetourMenu()
    {
        yield return new WaitForSeconds(4);
        YouWin.GetComponent<Image>().enabled = false;
        Sortie.SetActive(true);
    }
}
