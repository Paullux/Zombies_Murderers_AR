﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoOver : MonoBehaviour {

    private double playerCurrentFrame;
    private double playerFrameCount;
    private GameObject Controleur, videoPlayer;
    public bool videoOver = false;
    private bool first = true;
    private VideoPlayer VP;
    private AudioSource AS;
    public RawImage image;
    public RenderTexture render;

    // Use this for initialization
    void Start () {
        Controleur = GameObject.Find("MobileSingleStickControl");
        Controleur.SetActive(false);
        videoPlayer = GameObject.Find("Video Player");
        VP = videoPlayer.GetComponent<VideoPlayer>();
        AS = videoPlayer.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (first)
        {
            render = new RenderTexture(1600, 900, 24);
            image.texture = render;
            VP.targetTexture = render;

            VP.Play();
            AS.Play();
            first = false;
        }
        if (!videoOver)
        {
            playerCurrentFrame = VP.time;
            playerFrameCount = VP.clip.length;

            if (!(playerCurrentFrame < playerFrameCount - 0.1d) || Input.GetMouseButtonDown(0))
            {
                Controleur.SetActive(true);
                videoPlayer.SetActive(false);
                GetComponent<RawImage>().enabled = false;
                videoOver = true;
            }
        }
    }
}
