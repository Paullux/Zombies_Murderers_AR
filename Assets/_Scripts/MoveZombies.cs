﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveZombies : MonoBehaviour {

    private GameObject Soldier;
    private GameObject Humain;
    
    private Rigidbody rb;

    private Vector3 wantedPosition;
    private Vector3 newWantedPosition;
    private Vector3 wantedRotation;
    private Vector3 DirectionAPrendre;

    private bool zombiesAttack = false;

    private float VelociteduZombie;

    public float speed;
    public GameObject ZombieEnCours;

    private AudioSource Source;
    public AudioClip Eaten;

    private bool ZombieTouch = false;
    private bool ZombieEaten = false;



    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        Soldier = GameObject.Find("Soldier");
        newWantedPosition = Soldier.transform.position;
        Source = GetComponent<AudioSource>();
    }
    void Update()
    {
        foreach (GameObject Humain in GameObject.FindObjectsOfType(typeof(GameObject)))
        {
            if ((Humain.tag == "ZombieOrHumain" || Humain.name == "Soldier") && Humain != transform.gameObject)
            {
                if (Humain.transform.GetChild(0).gameObject.activeSelf)
                {
                    wantedPosition = Humain.transform.position;

                    if (Vector3.Distance(wantedPosition, transform.position) != 0 && Vector3.Distance(wantedPosition, transform.position) < Vector3.Distance(newWantedPosition, transform.position))
                    {
                        newWantedPosition = wantedPosition;
                    }
                }
            }
        }

        newWantedPosition = new Vector3(newWantedPosition.x, 0, newWantedPosition.z);

        if (!zombiesAttack)
        {
            if (ZombieTouch)
            {
                StartCoroutine("ZombieAttack");
            }
            else
            {
                ZombieEnCours.GetComponent<Animator>().ResetTrigger("attackOff");
            }
        }
        VelociteduZombie = GetComponent<PointDeVie>().getHP() / 400f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //newWantedPosition = new Vector3(newWantedPosition.x, 0f, newWantedPosition.z);

        DirectionAPrendre = newWantedPosition - transform.position;

        Vector3 direction = (new Vector3 (DirectionAPrendre.x, 0, DirectionAPrendre.z)).normalized;

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        if (ZombieEnCours.activeSelf)
        {
            StartCoroutine("ZombieWalk");
            rb.velocity = direction * VelociteduZombie;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg, transform.eulerAngles.z);
        }
        else
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }
    IEnumerator SoundBlessure()
    {
        ZombieEaten = true;
        while (ZombieTouch)
        {
            yield return new WaitForSeconds(1.025f);
            Source.PlayOneShot(Eaten, 1.0f);
        }
        ZombieEaten = false;
    }
    IEnumerator ZombieWalk()
    {
        ZombieEnCours.GetComponent<Animator>().ResetTrigger("Walk");
        ZombieEnCours.GetComponent<Animator>().SetTrigger("Walk");
        yield return null;
    }

    IEnumerator ZombieAttack(Collision other)
    {
        zombiesAttack = true;
        rb.angularVelocity = Vector3.zero;

        ZombieEnCours.GetComponent<Animator>().SetTrigger("attackOff");
        ZombieEnCours.GetComponent<Animator>().SetTrigger("attackOn");
        if (!ZombieEaten) StartCoroutine("SoundBlessure");
        if (other.gameObject.name == "Soldier")
        {
            other.gameObject.GetComponent<PointDeVie>().setHP(other.gameObject.GetComponent<PointDeVie>().getHP() - 4);
        }
        else
        {
            other.gameObject.GetComponent<PointDeVie>().setHP(other.gameObject.GetComponent<PointDeVie>().getHP() + 4);
        }
        yield return new WaitForSeconds(1);
        zombiesAttack = false;
    }
    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.name == "Soldier" || other.gameObject.tag == "ZombieOrHumain")
        {
            if(other.transform.GetChild(0).gameObject.activeSelf && other.gameObject != transform.gameObject)
            {
                if (!zombiesAttack)
                {
                    StartCoroutine("ZombieAttack", other);
                }
            }
        }
        rb.angularVelocity = Vector3.zero;
        rb.velocity = Vector3.zero;
    }
    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.name == "Soldier" || other.gameObject.tag == "ZombieOrHumain")
        {
            if (other.transform.GetChild(0).gameObject.activeSelf && other.gameObject != transform.gameObject)
            {
                StopCoroutine("ZombieAttack");
                ZombieEnCours.GetComponent<Animator>().SetTrigger("attackOff");
                zombiesAttack = false;
            }
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Vide") Destroy(gameObject);
    }
}
