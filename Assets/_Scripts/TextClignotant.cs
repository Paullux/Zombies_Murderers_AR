﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextClignotant : MonoBehaviour {

    private Text TextCligno;
    private bool first = true;

	// Use this for initialization
	void Start () {
        TextCligno = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(TextClignote());
    }
    IEnumerator TextClignote()
    {
        if (first)
        {
            first = false;
            TextCligno.GetComponent<Text>().enabled = true;
            yield return new WaitForSeconds(0.5f);
            TextCligno.GetComponent<Text>().enabled = false;
            yield return new WaitForSeconds(0.5f);
            first = true;
        }
    }
}
