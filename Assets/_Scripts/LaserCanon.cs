﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserCanon : MonoBehaviour {

    protected LineRenderer line;
	
    // Use this for initialization
	void Start () {
        line = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 rayDirection = transform.TransformDirection(Vector3.down);
        float rayRange = 500.0f;
        RaycastHit hit;
        Physics.Raycast(transform.position, rayDirection, out hit, rayRange);

        Vector3 hitPosition = transform.InverseTransformPoint(hit.point);

        line.SetPosition(0, Vector3.zero);

        if (hit.collider)
        {
            line.SetPosition(1, hitPosition);
        }
        else
        {
            line.SetPosition(1, new Vector3 (0, -10, 0));
        }
	}
}
