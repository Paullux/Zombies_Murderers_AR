﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBarreLife : MonoBehaviour {
    private GameObject CameraAR;
    private Vector3 wantedPosition;
    private Quaternion newWantedPosition;
    void Awake()
    {
        CameraAR = GameObject.Find("First Person Camera");
    }

    void FixedUpdate()
    {
        wantedPosition = CameraAR.transform.position - transform.position;
        newWantedPosition = Quaternion.Euler(wantedPosition.x, 0f, wantedPosition.z);

        transform.rotation = Quaternion.Slerp(transform.rotation, newWantedPosition, Time.deltaTime * 5.0f);
    }
}
