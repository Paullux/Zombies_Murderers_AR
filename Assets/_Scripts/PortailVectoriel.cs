﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortailVectoriel : MonoBehaviour {

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "First Person Camera")
        {
            SceneManager.LoadScene(3);
        }
    }

}
