﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveHumain : MonoBehaviour {
    
    private Vector3 dir;
    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision other)
    {
        dir = (transform.position - transform.forward).normalized * 1f;
        rb.velocity = dir;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg, transform.eulerAngles.z);
    }
    void OnCollisionExit(Collision other)
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }
}
