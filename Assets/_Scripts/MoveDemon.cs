﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDemon : MonoBehaviour {

    private GameObject Soldier;
    private GameObject Humain;

    private Rigidbody rb;

    private Vector3 wantedPosition;
    private Vector3 newWantedPosition;
    private Vector3 wantedRotation;
    private Vector3 DirectionAPrendre;

    private bool DeamonsAttack = false;

    private float VelociteduDeamon;

    public float speed;
    public GameObject DeamonEnCours;

    private AudioSource Source;
    public AudioClip Eaten;

    private bool DeamonTouch = false;
    private bool DeamonEaten = false;

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody>();
        Soldier = GameObject.Find("Soldier");
        newWantedPosition = Soldier.transform.position;
        Source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject Humain in GameObject.FindObjectsOfType(typeof(GameObject)))
        {
            if ((Humain.tag == "ZombieOrHumain" || Humain.name == "Soldier") && Humain != transform.gameObject)
            {
                if (Humain.transform.GetChild(0).gameObject.activeSelf)
                {
                    wantedPosition = Humain.transform.position;

                    if (Vector3.Distance(wantedPosition, transform.position) != 0 && Vector3.Distance(wantedPosition, transform.position) < Vector3.Distance(newWantedPosition, transform.position))
                    {
                        newWantedPosition = wantedPosition;
                    }
                }
            }
        }

        newWantedPosition = new Vector3(newWantedPosition.x, 0, newWantedPosition.z);

        if (!DeamonsAttack)
        {
            if (DeamonTouch)
            {
                StartCoroutine("DeamonAttack");
            }
            else
            {
                DeamonEnCours.GetComponent<Animator>().ResetTrigger("attackOff");
            }
        }
        VelociteduDeamon = GetComponent<PointDeVie>().getHP() / 750f;
    }
    void FixedUpdate()
    {

        //newWantedPosition = new Vector3(newWantedPosition.x, 0f, newWantedPosition.z);

        DirectionAPrendre = newWantedPosition - transform.position;

        Vector3 direction = (new Vector3(DirectionAPrendre.x, 0, DirectionAPrendre.z)).normalized;

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        if (DeamonEnCours.activeSelf)
        {
            StartCoroutine("DeamonWalk");
            rb.velocity = direction * VelociteduDeamon;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg, transform.eulerAngles.z);
        }
        else
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }
    IEnumerator SoundBlessure()
    {
        DeamonEaten = true;
        while (DeamonTouch)
        {
            yield return new WaitForSeconds(1.025f);
            Source.PlayOneShot(Eaten, 1.1f);
        }
        DeamonEaten = false;
    }
    IEnumerator DeamonWalk()
    {
        DeamonEnCours.GetComponent<Animator>().ResetTrigger("Walk");
        DeamonEnCours.GetComponent<Animator>().SetTrigger("Walk");
        yield return null;
    }

    IEnumerator DeamonAttack(Collision other)
    {
        DeamonsAttack = true;
        rb.angularVelocity = Vector3.zero;

        DeamonEnCours.GetComponent<Animator>().SetTrigger("attackOff");
        DeamonEnCours.GetComponent<Animator>().SetTrigger("attackOn");
        if (!DeamonEaten) StartCoroutine("SoundBlessure");
        if (other.gameObject.name == "Soldier")
        {
            other.gameObject.GetComponent<PointDeVie>().setHP(other.gameObject.GetComponent<PointDeVie>().getHP() - 4);
        }
        else
        {
            other.gameObject.GetComponent<PointDeVie>().setHP(other.gameObject.GetComponent<PointDeVie>().getHP() + 4);
        }
        yield return new WaitForSeconds(1);
        DeamonsAttack = false;
    }
    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.name == "Soldier" || other.gameObject.tag == "ZombieOrHumain")
        {
            if (other.transform.GetChild(0).gameObject.activeSelf && other.gameObject != transform.gameObject)
            {
                if (!DeamonsAttack)
                {
                    StartCoroutine("DeamonAttack", other);
                }
            }
        }
        rb.angularVelocity = Vector3.zero;
        rb.velocity = Vector3.zero;
    }
    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.name == "Soldier" || other.gameObject.tag == "ZombieOrHumain")
        {
            if (other.transform.GetChild(0).gameObject.activeSelf && other.gameObject != transform.gameObject)
            {
                StopCoroutine("DeamonAttack");
                DeamonEnCours.GetComponent<Animator>().SetTrigger("attackOff");
                DeamonsAttack = false;
            }
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Vide") Destroy(gameObject);
    }
}
