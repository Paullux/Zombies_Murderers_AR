﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitVideo : MonoBehaviour {

    private MeshRenderer MyPortail;
	// Use this for initialization
	void Start () {
        MyPortail = GetComponent<MeshRenderer>();
        MyPortail.enabled = false;
        StartCoroutine(StartRenderer());
	}
    IEnumerator StartRenderer()
    {
        yield return new WaitForSeconds(1.5f);
        MyPortail.enabled = true;
    }
}
