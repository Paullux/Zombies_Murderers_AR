﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore.Examples.HelloAR;

public class TouchScreen : MonoBehaviour {
    private Text Touch;
    public GameObject SnackBar;
    public bool Intancie;
    // Use this for initialization
    void Start () {
        Touch = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        Touch.enabled = !SnackBar.activeSelf && !Intancie;
	}
}
