﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MoveCible : MonoBehaviour {
    private float x, y;
    private Vector3 NewPosition;
    private bool fire;
    public GameObject Point2;
    public Camera camera;
    public GameObject Eclair;
    public GameObject SnackBar;
    private Image Cible;
    private GameObject Monstre;
    private GameObject DontKillHumans;
    private bool firstHuman = true;
    private AudioSource SourceAudio;

    // Use this for initialization
    void Start () {
        NewPosition = transform.position;
        Cible = GetComponent<Image>();
        DontKillHumans = GameObject.Find("DontKillHumans");
        SourceAudio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {

        Cible.enabled = !(SnackBar.activeSelf);
        
        x = CrossPlatformInputManager.GetAxis("Horizontal");
        y = CrossPlatformInputManager.GetAxis("Vertical");

        NewPosition = transform.position + new Vector3(x, y, 0f) * 50f;

        x = NewPosition.x;
        y = NewPosition.y;

        if (x < 0) x = 0;
        if (x > Screen.width) x = Screen.width;
        if (y < 0) y = 0;
        if (y > Screen.height) y = Screen.height;

        NewPosition.x = x;
        NewPosition.y = y;
        NewPosition.z = 0f;

        transform.position = NewPosition;
        fire = CrossPlatformInputManager.GetButton("Fire2");

        if (fire)
        {
            Eclair.SetActive(true);
            float rayRange = 500.0f;
            Ray rayDir = camera.ScreenPointToRay(NewPosition);
            RaycastHit hit;

            if (Physics.Raycast(rayDir, out hit, rayRange))
            {
                if (hit.collider)
                {
                    Point2.transform.position = hit.point;
                    if (hit.collider.name == "Level (0)")
                    {
                        StartCoroutine(ChangeLevel(1));
                    }
                    if (hit.collider.name == "Level (1)")
                    {
                        StartCoroutine(ChangeLevel(2));
                    }
                    if (hit.collider.name == "Level (2)")
                    {
                        StartCoroutine(ChangeLevel(3));
                    }
                    if (hit.collider.tag == "ZombieOrHumain")
                    {
                        Monstre = hit.collider.gameObject;
                        Monstre.GetComponent<PointDeVie>().setHP(Monstre.GetComponent<PointDeVie>().getHP() - 8);
                        if (hit.collider.transform.GetComponent<ZombiesOrHumain>().humain && firstHuman)
                        {
                            firstHuman = false;
                            hit.collider.gameObject.GetComponent<PointDeVie>().setHP(250);
                            StartCoroutine(DoNotKillHumans());
                        }
                    }
                    if (hit.collider.name == "Collider1")
                    {
                        Monstre = GameObject.Find("Reptile");
                        Monstre.GetComponent<PointDeVie>().setHP(Monstre.GetComponent<PointDeVie>().getHP() - 8);
                    }
                    if (hit.collider.name == "Collider2")
                    {
                        Monstre = GameObject.Find("Reptile");
                        Monstre.GetComponent<PointDeVie>().setHP(Monstre.GetComponent<PointDeVie>().getHP() - 250);
                    }
                }
                else
                {
                    StopCoroutine(ChangeLevel(0));
                    Point2.transform.position = 10 * transform.TransformDirection(Vector3.forward);
                }
            }
        }
        else
        {
            Eclair.SetActive(false);
        }
    }
    IEnumerator ChangeLevel (int nextLevel)
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(nextLevel);
    }
    IEnumerator DoNotKillHumans()
    {
        DontKillHumans.GetComponent<Image>().enabled = true;
        SourceAudio.Play();
        yield return new WaitForSeconds(1f);
        DontKillHumans.GetComponent<Image>().enabled = false;
        yield return new WaitForSeconds(.5f);
        firstHuman = true;
    }
}
