﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCoreInternal;

public class ZombiesOrHumainlvl3 : MonoBehaviour {

    public GameObject DemonIci;
    public GameObject ZombieIci;
    public GameObject HumainIci;
    public bool humain;
    private float lighting;
    private float PTVZombie;
    private Rigidbody rb;

    void Start ()
    {
        humain = false;
        rb = GetComponent<Rigidbody>();
        GetComponent<MoveHumain>().enabled = false;
        GetComponent<MoveDemonlvl3>().enabled = false;
    }

    void Update () {

        PTVZombie = GetComponent<PointDeVie>().getHP();

        lighting = Frame.LightEstimate.PixelIntensity;

        if (PTVZombie > 0 && PTVZombie <=100f && lighting <= 0.65f)
        {
            DemonIci.SetActive(false);
            ZombieIci.SetActive(true);
            HumainIci.SetActive(false);
            humain = false;
            GetComponent<MoveHumain>().enabled = false;
            GetComponent<MoveZombieslvl3>().enabled = true;
            GetComponent<MoveDemonlvl3>().enabled = false;
        }
        else
        {
            if (PTVZombie <= 0)
            {
                DemonIci.SetActive(false);
                ZombieIci.SetActive(false);
                HumainIci.SetActive(true);
                humain = true;
                rb.angularVelocity = Vector3.zero;
                rb.velocity = Vector3.zero;
                GetComponent<MoveZombieslvl3>().enabled = false;
                GetComponent<MoveHumain>().enabled = true;
                GetComponent<MoveDemonlvl3>().enabled = false;
            }
            else if (PTVZombie > 100)
            {
                DemonIci.SetActive(true);
                ZombieIci.SetActive(false);
                HumainIci.SetActive(false);
                humain = false;
                GetComponent<MoveZombieslvl3>().enabled = false;
                GetComponent<MoveHumain>().enabled = false;
                GetComponent<MoveDemonlvl3>().enabled = true;
            }
        }
    }
}
