﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Time2time : MonoBehaviour {

    public Text counterText;
    public float seconds, minutes;

    private bool FaitNuit;



    void Start()
    {

        counterText = GetComponent<Text>() as Text;

        FaitNuit = true;
    }

    void Update () {

        if (Time.timeScale == 1)
        {
            minutes = (int)(20f * Time.time / 60f);

            while (minutes >= 24)
            {
                minutes -= 24;
                if (Time.timeScale != 1) break;
            }

            seconds = (int)(20f * Time.time % 60f);
        }
        counterText.text = minutes.ToString("00") + " h " + seconds.ToString("00");

        if (minutes == 8 && seconds >= 0 && seconds <= 25)
        {

            FaitNuit = false;
        }
        if (minutes == 20 && seconds >= 0 && seconds <= 25)
        {

            FaitNuit = true;
        }
        if (FaitNuit)
        {
            PlayerPrefs.SetInt("NuitNoir", 1);
        }
        else
        {
            PlayerPrefs.SetInt("NuitNoir", 0);
        }
    }
}
