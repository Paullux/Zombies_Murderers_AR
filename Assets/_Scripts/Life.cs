﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Life : MonoBehaviour {

    public int PointOfLife;
    public Texture2D emptyProgressBar; // Set this in inspector.
    public Texture2D fullProgressBar; // Set this in inspector.
    public Vector2 pos;
    public Vector2 size;
    private bool sup1000 = false;
    //public GameObject Cible;
    public Scrollbar ProgressBarre;

    public void Barre()
    {
        OnGUI();
    }

    void OnGUI()
    {
        //GUI.DrawTexture(new Rect(pos.x, pos.y, size.x, size.y), emptyProgressBar);
        //GUI.DrawTexture(new Rect(pos.x, pos.y, 0.01f * PointOfLife, size.y), fullProgressBar);
    }
    void Update()
    {
        PointOfLife = GetComponent<PointDeVie>().getHP();

        if (PointOfLife > 260) sup1000 = true;

        if (!sup1000)
        {
            if (PointOfLife <= 100) ProgressBarre.size = 0.01f * PointOfLife;
            if (PointOfLife > 100) ProgressBarre.size = 0.004f * PointOfLife;
        }
        else
        { 
            ProgressBarre.size = 0.001f * PointOfLife;
        }
    }
}