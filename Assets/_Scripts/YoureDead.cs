﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class YoureDead : MonoBehaviour {
    public AudioClip YouReDead;
	public GameObject myUI;
    private bool first = true;
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (myUI.GetComponent<PointDeVie>().getHP() <= 0 && first)
        {
            first = false;
            GetComponent<Image>().enabled = true;
            GetComponent<AudioSource>().PlayOneShot(YouReDead, 1f);
            StartCoroutine(RetourMenu());
        }

	}
    IEnumerator RetourMenu()
    {
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(0);
    }
}
