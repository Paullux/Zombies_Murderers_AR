﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CountOfZombieslvl3 : MonoBehaviour
{

    private int NbZombies, NbHumain;
    private AudioSource SourceAudio;
    public AudioClip WinSound;
    private GameObject YouWin;
    private bool First = true;

    // Use this for initialization
    void Start()
    {
        SourceAudio = GetComponent<AudioSource>();
        YouWin = GameObject.Find("YouWin");
    }

    // Update is called once per frame
    void Update()
    {
        NbZombies = 0;
        NbHumain = 0;

        foreach (GameObject Humain in FindObjectsOfType(typeof(GameObject)))
        {
            if (Humain.tag == "ZombieOrHumain")
            {
                NbZombies += 1;
                if (Humain.GetComponent<ZombiesOrHumainlvl3>().humain) NbHumain += 1;
            }
        }
        if (NbZombies != 0 && NbZombies == NbHumain)
        {
            YouWin.GetComponent<Image>().enabled = true;
            if (First)
            {
                SourceAudio.PlayOneShot(WinSound, 1f);
                First = false;
            }
            StartCoroutine("RetourMenu");
        }
        else
        {
            YouWin.GetComponent<Image>().enabled = false;
        }
    }
    IEnumerator RetourMenu ()
    {
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(0);
    }
}

