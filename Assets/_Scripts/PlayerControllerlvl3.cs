﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class PlayerControllerlvl3 : MonoBehaviour
{

    private Rigidbody rb;
    private Animator anim;
    private GameObject Player;
    private GameObject Flamme;
    private AudioSource SourceAudio;
    private int PTDV;
    public AudioClip FusilFire;
    private GameObject YouReDead;
    private GameObject DontKillHumans;
    public AudioClip DeadSound;
    private bool killed = false;
    private GameObject cam;
    
    public bool FireEnCours = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Player = GameObject.Find("ZombieOrHumain");
        SourceAudio = GetComponent<AudioSource>();
        YouReDead = GameObject.Find("YouReDead");
        DontKillHumans = GameObject.Find("DontKillHumans");
        cam = GameObject.Find("First Person Camera");
    }

    void Update()
    {
        bool fire = CrossPlatformInputManager.GetButton("Fire2");
        if (fire)
        {
            if (!FireEnCours)
            {
                StartCoroutine("Fire");
            }
        }
        PTDV = GetComponent<PointDeVie>().getHP();
        if (PTDV <= 0 && !killed)
        {
            killed = true;
            anim.SetTrigger("Dead");
            YouReDead.GetComponent<Image>().enabled = true;
            SourceAudio.PlayOneShot(DeadSound, 1f);
            StartCoroutine(RetourMenu());
        }
    }
    IEnumerator RetourMenu()
    {
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(0);
    }
}