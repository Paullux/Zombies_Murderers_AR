﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class ButtonResetAndQuit : MonoBehaviour {

	void Update () {
        bool reset = CrossPlatformInputManager.GetButtonDown("Reset");
        if (reset)
        {
            int currentScene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(currentScene);
        }
        bool quit = CrossPlatformInputManager.GetButtonDown("Quit");
        if (quit)
        {
            Application.Quit();
#if UNITY_ANDROID
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
            jo.Call("closeActivity");
#endif
        }
        bool menu = CrossPlatformInputManager.GetButtonDown("Menu");
        if (menu)
        {
            SceneManager.LoadScene(0);
        }
    }
}
